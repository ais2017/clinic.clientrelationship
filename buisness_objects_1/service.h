#ifndef SERVICE_H
#define SERVICE_H



class Service
{
private:
    int id;
    bool status;
    string name;
    int price;
    struct tm date;
public:
    Service(int id_n = -1, int p = 0, string n = "", bool stat = false)
    {
        if (id_n < 0) {
            throw logic_error("id < 0");
        }
        id = id_n;
        status = stat;
        if (stat) {
            time_t now;
            time(&now);
            date = *localtime(&now);
        } else {
            date = tm();
        }
        name = n;
        if (p < 0 ){
            throw logic_error("price < 0");
        }
        price = p;
    }
    void Pay();
    ~Service();
    int GetId();
    bool GetStatus();
    string GetName();
    int GetPrice();
    struct tm GetDate();
};

#endif // SERVICE_H
