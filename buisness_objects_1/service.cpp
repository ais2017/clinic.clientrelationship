#include <stdexcept>
#include <time.h>
#include <string>


using namespace std;

#include "service.h"

Service::~Service(){}

void Service::Pay(){
    if (!status){
        status = true;
        time_t now;
        time(&now);
        date = *localtime(&now);
    }
    return;
}

struct tm Service::GetDate(){return date;}

bool Service::GetStatus(){return status;}

string Service::GetName(){return name;}

int Service::GetPrice(){return price;}

int Service::GetId(){return id;}

