#include <string>
#include <vector>
#include <stdexcept>
#include <time.h>
using namespace std;

#include "service.h"
#include "patient.h"

Patient::Patient(int id_n, string sur, string n, string pat, Birth_date dat)
{
    if (id_n < 0){
        throw logic_error("id < 0");
    }
    id = id_n;
    if (sur == ""){
        throw logic_error("No surname");
    }
    surname = sur;
    if (n == ""){
        throw logic_error("No surname");
    }
    name = n;
    if (pat == ""){
        throw logic_error("No surname");
    }
    patronymic = pat;
    services = vector<Service*>();

    time_t now;
    time(&now);
    struct tm t = *localtime(&now);

    if ((dat.year < 1) || (dat.month < 1) || (dat.day < 1) || (dat.month > 12) || (dat.day > 31) || (dat.year > t.tm_year + 1900)){
        throw logic_error("wrong birth_date");
    }

    date = dat;
}

Patient::~Patient()
{
    for (int i = 0; i < services.size(); i++){
        delete services[i];
    }
};

void Patient::DeleteData()
{
    date.day = 0;
    date.month = 0;
    date.year = 0;
    name = "";
    surname = "";
    patronymic = "";
    return;
}

void Patient::AddService(Service* s)
{
    services.push_back(s);
    return;
}

int Patient::GetId(){return id;}

void Patient::ChangeName(string s){s == "" ? throw logic_error("No surname") : name = s; return;}

void Patient::ChangePatronymic(string s){s == "" ? throw logic_error("No surname") : patronymic = s; return;}

void Patient::ChangeSurname(string s){s == "" ? throw logic_error("No surname") : surname = s; return;}

string Patient::GetName(){return name;}

string Patient::GetPatronymic(){return patronymic;}

string Patient::GetSurname(){return surname;}

Birth_date Patient::GetDate(){return date;}

vector<Service*> Patient::GetServices(){return services;}
