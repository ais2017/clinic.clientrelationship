#include <QtTest>
#include <stdexcept>
#include <string>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

#include "../service.h"

// add necessary includes here

bool tm_compare(tm t1, tm t2){
    return (t1.tm_hour == t2.tm_hour)&&(t1.tm_isdst == t2.tm_isdst)&&(t1.tm_mday == t2.tm_mday)&&(abs(t1.tm_sec - t2.tm_sec) < 2)&&
            (t1.tm_mon == t2.tm_mon)&&(t1.tm_min == t2.tm_min)&&(t1.tm_wday == t2.tm_wday)&&(t1.tm_yday == t2.tm_yday)&&(t1.tm_year == t2.tm_year);
}

class service_test : public QObject
{
    Q_OBJECT

public:
    service_test();
    ~service_test();

private slots:
    void Test_Constructor1();
    void Test_Constructor2();
    void Test_Constructor3();
    void Test_Constructor4();
    void Test_Constructor5();
    void Test_Constructor6();
    void Test_Constructor7();
    //void Test_Constructor8();
    void Test_Pay1();
    void Test_Pay2();
    //void Test_Pay3();
    //void Test_Pay4();
    //void Test_Pay5();
    //void Test_Pay();

};

service_test::service_test()
{

}

service_test::~service_test()
{

}

void service_test::Test_Constructor1()
{
    srand(time(NULL));
    int id = rand();
    int stat = false;
    int p = rand();
    struct tm t = tm();
    string n = "name";
    Service service_1(id, p, n, stat);
    QCOMPARE(id,service_1.GetId());
    QCOMPARE(stat,service_1.GetStatus());
    QCOMPARE(n,service_1.GetName());
    QCOMPARE(p,service_1.GetPrice());
    QVERIFY(tm_compare(t,service_1.GetDate()));
}

void service_test::Test_Constructor7()
{
    srand(time(NULL));
    int id = rand();
    int stat = true;
    int p = rand();
    time_t now;
    time(&now);
    struct tm t = *localtime(&now);
    string n = "name";
    Service service_1(id, p, n, stat);
    QCOMPARE(id,service_1.GetId());
    QCOMPARE(stat,service_1.GetStatus());
    QCOMPARE(n,service_1.GetName());
    QCOMPARE(p,service_1.GetPrice());
    QVERIFY(tm_compare(t,service_1.GetDate()));
}

void service_test::Test_Constructor2()
{
    srand(time(NULL));
    int id = rand();
    struct tm t = tm();
    Service service_1(id);
    QCOMPARE(id,service_1.GetId());
    QCOMPARE(false,service_1.GetStatus());
    QCOMPARE("",service_1.GetName());
    QCOMPARE(0,service_1.GetPrice());
    QVERIFY(tm_compare(t,service_1.GetDate()));
}

void service_test::Test_Constructor3()
{
    srand(time(NULL));
    int id = rand();
    int stat = rand() % 2;
    int p = -1*rand() - 1;
    string n = "name";
    QVERIFY_EXCEPTION_THROWN(Service service_1(id, p, n, stat), logic_error);
}

void service_test::Test_Constructor4()
{
    srand(time(NULL));
    int id = -1*rand() - 1;
    int stat = rand() % 2;
    int p = rand();
    string n = "name";
    QVERIFY_EXCEPTION_THROWN(Service service_1(id, p, n, stat), logic_error);
}

void service_test::Test_Constructor5()
{
    srand(time(NULL));
    int id = rand();
    int p = rand();
    struct tm t = tm();
    Service service_1(id, p);
    QCOMPARE(id,service_1.GetId());
    QCOMPARE(false,service_1.GetStatus());
    QCOMPARE("",service_1.GetName());
    QCOMPARE(p,service_1.GetPrice());
    QVERIFY(tm_compare(t,service_1.GetDate()));
}

void service_test::Test_Constructor6()
{
    srand(time(NULL));
    int id = rand();
    int p = rand();
    struct tm t = tm();
    string n = "name";
    Service service_1(id, p, n);
    QCOMPARE(id,service_1.GetId());
    QCOMPARE(false,service_1.GetStatus());
    QCOMPARE(n,service_1.GetName());
    QCOMPARE(p,service_1.GetPrice());
    QVERIFY(tm_compare(t,service_1.GetDate()));
}


void service_test::Test_Pay1()
{
    srand(time(NULL));
    int id = rand();
    Service service_1(id);
    QCOMPARE(false,service_1.GetStatus());
    time_t now;
    time(&now);
    struct tm t = *localtime(&now);
    service_1.Pay();
    QCOMPARE(true,service_1.GetStatus());
    QVERIFY(tm_compare(t,service_1.GetDate()));
}

void service_test::Test_Pay2()
{
    srand(time(NULL));
    int id = rand();
    Service service_1(id,true);
    struct tm now = tm();
    service_1.Pay();
    QCOMPARE(true,service_1.GetStatus());
    QVERIFY(!tm_compare(now,service_1.GetDate()));
}

QTEST_APPLESS_MAIN(service_test)

#include "tst_service_test.moc"
