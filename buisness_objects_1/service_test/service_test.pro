QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_service_test.cpp \
    ../service.cpp

HEADERS += \
    ../service.h
