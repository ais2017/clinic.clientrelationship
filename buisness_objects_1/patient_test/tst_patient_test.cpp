#include <QtTest>
#include <string>
#include <vector>
#include <stdexcept>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

#include "../patient.h"
#include "../service.h"



// add necessary includes here

bool birth_compare(Birth_date date1, Birth_date date2){return (date1.day == date2.day)&&(date1.month == date2.month)&&(date1.year == date2.year);}

bool tm_compare(tm t1, tm t2){
    return (t1.tm_hour == t2.tm_hour)&&(t1.tm_isdst == t2.tm_isdst)&&(t1.tm_mday == t2.tm_mday)&&(abs(t1.tm_sec - t2.tm_sec) < 2)&&
            (t1.tm_mon == t2.tm_mon)&&(t1.tm_min == t2.tm_min)&&(t1.tm_wday == t2.tm_wday)&&(t1.tm_yday == t2.tm_yday)&&(t1.tm_year == t2.tm_year);
}

class patient_test : public QObject
{
    Q_OBJECT

public:
    patient_test();
    ~patient_test();

private slots:
    void Test_Constructor1();
    void Test_Constructor2();
    void Test_Constructor3();
    void Test_Constructor4();
    void Test_Constructor5();
    void Test_Constructor6();
    void Test_Constructor7();
    void Test_Constructor8();
    void Test_Constructor9();
    void Test_Constructor10();
    void Test_Constructor11();
    void Test_ChangeName1();
    void Test_ChangeName2();
    void Test_ChangeName3();
    void Test_ChangeSurname1();
    void Test_ChangeSurname2();
    void Test_ChangeSurname3();
    void Test_ChangePatronymic1();
    void Test_ChangePatronymic2();
    void Test_ChangePatronymic3();
    void Test_AddService1();
    void Test_AddService2();
    void Test_DeleteData();

};

patient_test::patient_test()
{

}

patient_test::~patient_test()
{

}

void patient_test::Test_Constructor1()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient person = Patient(id, sur, n, pat, date);
    QCOMPARE(id,person.GetId());
    QCOMPARE(n,person.GetName());
    QCOMPARE(sur,person.GetSurname());
    QCOMPARE(pat,person.GetPatronymic());
    QCOMPARE(0,person.GetServices().size());
    QVERIFY(birth_compare(date, person.GetDate()));
}

void patient_test::Test_Constructor2()
{
    srand(time(NULL));
    int id = -1*rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    QVERIFY_EXCEPTION_THROWN(Patient person = Patient(id, sur, n, pat, date), logic_error);
}

void patient_test::Test_Constructor3()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = 0;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    QVERIFY_EXCEPTION_THROWN(Patient person = Patient(id, sur, n, pat, date), logic_error);
}

void patient_test::Test_Constructor4()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 32;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    QVERIFY_EXCEPTION_THROWN(Patient person = Patient(id, sur, n, pat, date), logic_error);
}

void patient_test::Test_Constructor5()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = 0;
    date.year = rand() % 2018 + 1;
    QVERIFY_EXCEPTION_THROWN(Patient person = Patient(id, sur, n, pat, date), logic_error);
}

void patient_test::Test_Constructor6()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 13;
    date.year = rand() % 2018 + 1;
    QVERIFY_EXCEPTION_THROWN(Patient person = Patient(id, sur, n, pat, date), logic_error);
}

void patient_test::Test_Constructor7()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = 0;
    QVERIFY_EXCEPTION_THROWN(Patient person = Patient(id, sur, n, pat, date), logic_error);
}

void patient_test::Test_Constructor8()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = 2020;
    QVERIFY_EXCEPTION_THROWN(Patient person = Patient(id, sur, n, pat, date), logic_error);
}

void patient_test::Test_Constructor9()
{
    srand(time(NULL));
    int id = rand();
    string n = "";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    QVERIFY_EXCEPTION_THROWN(Patient person = Patient(id, sur, n, pat, date), logic_error);
}

void patient_test::Test_Constructor10()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    QVERIFY_EXCEPTION_THROWN(Patient person = Patient(id, sur, n, pat, date), logic_error);
}

void patient_test::Test_Constructor11()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    QVERIFY_EXCEPTION_THROWN(Patient person = Patient(id, sur, n, pat, date), logic_error);
}

void patient_test::Test_ChangeName1()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient person = Patient(id, sur, n, pat, date);
    QCOMPARE(n,person.GetName());
    n = "name1";
    person.ChangeName(n);
    QCOMPARE(n,person.GetName());
}

void patient_test::Test_ChangeName2()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient person = Patient(id, sur, n, pat, date);
    QCOMPARE(n,person.GetName());
    person.ChangeName(n);
    QCOMPARE(n,person.GetName());
}

void patient_test::Test_ChangeName3()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient person = Patient(id, sur, n, pat, date);
    QVERIFY_EXCEPTION_THROWN(person.ChangeName(""), logic_error);
}

void patient_test::Test_ChangeSurname1()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient person = Patient(id, sur, n, pat, date);
    QCOMPARE(sur,person.GetSurname());
    sur = "surname1";
    person.ChangeSurname(sur);
    QCOMPARE(sur,person.GetSurname());
}

void patient_test::Test_ChangeSurname2()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient person = Patient(id, sur, n, pat, date);
    QCOMPARE(sur,person.GetSurname());
    person.ChangeSurname(sur);
    QCOMPARE(sur,person.GetSurname());
}

void patient_test::Test_ChangeSurname3()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient person = Patient(id, sur, n, pat, date);
    QVERIFY_EXCEPTION_THROWN(person.ChangeSurname(""), logic_error);
}

void patient_test::Test_ChangePatronymic1()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient person = Patient(id, sur, n, pat, date);
    QCOMPARE(pat,person.GetPatronymic());
    pat = "patronymic1";
    person.ChangePatronymic(pat);
    QCOMPARE(pat,person.GetPatronymic());
}

void patient_test::Test_ChangePatronymic2()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient person = Patient(id, sur, n, pat, date);
    QCOMPARE(pat,person.GetPatronymic());
    person.ChangePatronymic(pat);
    QCOMPARE(pat,person.GetPatronymic());
}

void patient_test::Test_ChangePatronymic3()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient person = Patient(id, sur, n, pat, date);
    QVERIFY_EXCEPTION_THROWN(person.ChangePatronymic(""), logic_error);
}

void patient_test::Test_AddService1()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient person = Patient(id, sur, n, pat, date);
    int id1 = rand();
    int stat = false;
    int p = rand();
    n = "name";
    struct tm t = tm();
    Service* service_1 = new Service(id1, p, n, stat);
    person.AddService(service_1);
    QVERIFY(person.GetServices().size() > 0);
    QCOMPARE(person.GetServices()[0]->GetId(),id1);
    QCOMPARE(person.GetServices()[0]->GetStatus(),stat);
    QCOMPARE(person.GetServices()[0]->GetPrice(),p);
    QCOMPARE(person.GetServices()[0]->GetName(),n);
    QVERIFY(tm_compare(t,person.GetServices()[0]->GetDate()));
}

void patient_test::Test_AddService2()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient person = Patient(id, sur, n, pat, date);
    int id1 = rand();
    int stat = false;
    int p = rand();
    n = "name";
    Service* service_1 = new Service(id1, p, n, stat);
    int k = rand() % 10 + 10;
    for(int i = 0; i < k; i++){
        person.AddService(service_1);
    }
    QCOMPARE(person.GetServices().size(),k);
}

void patient_test::Test_DeleteData()
{
    srand(time(NULL));
    int id = rand();
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date(), date1 = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient person = Patient(id, sur, n, pat, date);
    QCOMPARE(n,person.GetName());
    QCOMPARE(sur,person.GetSurname());
    QCOMPARE(pat,person.GetPatronymic());
    person.DeleteData();
    QCOMPARE("",person.GetName());
    QCOMPARE("",person.GetSurname());
    QCOMPARE("",person.GetPatronymic());
    QVERIFY(birth_compare(date1, person.GetDate()));
}

QTEST_APPLESS_MAIN(patient_test)

#include "tst_patient_test.moc"
