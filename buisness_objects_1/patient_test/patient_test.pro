QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_patient_test.cpp \
    ../patient.cpp \
    ../service.cpp

HEADERS += \
    ../patient.h \
    ../service.h
