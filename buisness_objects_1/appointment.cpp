#include <vector>
#include <string>
#include <stdexcept>
#include <time.h>

using namespace std;

#include "service.h"
#include "patient.h"
#include "appointment.h"

Appointment::Appointment(int id_n, Patient* per, int id1, struct tm t){
    if (id_n < 0){
        throw logic_error("id < 0");
    }
    id = id_n;
    person = per;
    if (id1 < 0){
        throw logic_error("doctor_id < 0");
    }
    doctor_id = id1;
    deny = false;
    time_t now;
    time(&now);
    struct tm date = *localtime(&now);
    if (t.tm_year < date.tm_year) {
        throw logic_error("wrong date");
    } else if (t.tm_year == date.tm_year){
        if (t.tm_mon < date.tm_mon){
            throw logic_error("wrong date");
        } else if (t.tm_mon == date.tm_mon){
            if (t.tm_mday < date.tm_mday){
                throw logic_error("wrong date");
            } else if(t.tm_mday == date.tm_mday){
                if (t.tm_hour < date.tm_hour){
                    throw logic_error("wrong date");
                } else if (t.tm_hour == date.tm_hour){
                    if (t.tm_min < date.tm_min){
                        throw logic_error("wrong date");
                    }
                }
            }
        }
    }
    ap_time = t;
}

Appointment::~Appointment()
{
    delete person;
}

void Appointment::ToDeny(){deny = true; return;}

bool Appointment::Deny(){return deny;}

int Appointment::GetId(){return id;}

int Appointment::GetId_D(){return doctor_id;}

Patient* Appointment::GetPerson(){return person;}

struct tm Appointment::GetTime(){return ap_time;}
