QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_appointmen_test.cpp \
    ../appointment.cpp \
    ../patient.cpp \
    ../service.cpp

HEADERS += \
    ../appointment.h \
    ../patient.h \
    ../service.h
