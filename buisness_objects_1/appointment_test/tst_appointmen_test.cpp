#include <QtTest>
#include <string>
#include <vector>
#include <stdexcept>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

#include<../service.h>
#include <../patient.h>
#include <../appointment.h>
// add necessary includes here

bool birth_compare(Birth_date date1, Birth_date date2){return (date1.day == date2.day)&&(date1.month == date2.month)&&(date1.year == date2.year);}

bool tm_compare(tm t1, tm t2){
    return (t1.tm_hour == t2.tm_hour)&&(t1.tm_isdst == t2.tm_isdst)&&(t1.tm_mday == t2.tm_mday)&&(abs(t1.tm_sec - t2.tm_sec) < 2)&&
            (t1.tm_mon == t2.tm_mon)&&(t1.tm_min == t2.tm_min)&&(t1.tm_wday == t2.tm_wday)&&(t1.tm_yday == t2.tm_yday)&&(t1.tm_year == t2.tm_year);
}

class appointmen_test : public QObject
{
    Q_OBJECT

public:
    appointmen_test();
    ~appointmen_test();

private slots:
    void Test_Constructor1();
    void Test_Constructor2();
    void Test_Constructor3();
    void Test_Constructor4();
    void Test_Constructor5();
    void Test_Constructor6();
    void Test_Constructor7();
    void Test_Constructor8();
    void Test_Constructor9();
    void Test_ToDeny1();
    void Test_ToDeny2();
};

appointmen_test::appointmen_test()
{

}

appointmen_test::~appointmen_test()
{

}

void appointmen_test::Test_Constructor1()
{
    srand(time(NULL));
    int id1 = rand();
    int id2 = rand();
    int id = rand();
    time_t now;
    time(&now);
    struct tm t = *localtime(&now);
    t.tm_hour += 1;
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient* person = new Patient(id, sur, n, pat, date);
    Appointment ap(id1, person, id2, t);
    QCOMPARE(id1, ap.GetId());
    QCOMPARE(id2, ap.GetId_D());
    QVERIFY(tm_compare(t, ap.GetTime()));
    QCOMPARE(id, ap.GetPerson()->GetId());
    QCOMPARE(n, ap.GetPerson()->GetName());
    QCOMPARE(sur, ap.GetPerson()->GetSurname());
    QCOMPARE(pat, ap.GetPerson()->GetPatronymic());
    QCOMPARE(false, ap.Deny());
    QVERIFY(birth_compare(date, ap.GetPerson()->GetDate()));
    QCOMPARE(0, ap.GetPerson()->GetServices().size());
}

void appointmen_test::Test_Constructor2()
{
    srand(time(NULL));
    int id1 = rand();
    int id2 = rand();
    int id = rand();
    time_t now;
    time(&now);
    struct tm t = *localtime(&now);
    t.tm_mon -= 1;
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient* person = new Patient(id, sur, n, pat, date);
    QVERIFY_EXCEPTION_THROWN(Appointment ap(id1, person, id2, t), logic_error);
}

void appointmen_test::Test_Constructor3()
{
    srand(time(NULL));
    int id1 = -1*rand() - 1;
    int id2 = rand();
    int id = rand();
    time_t now;
    time(&now);
    struct tm t = *localtime(&now);
    t.tm_mon += 1;
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient* person = new Patient(id, sur, n, pat, date);
    QVERIFY_EXCEPTION_THROWN(Appointment ap(id1, person, id2, t), logic_error);
}

void appointmen_test::Test_Constructor4()
{
    srand(time(NULL));
    int id1 = rand();
    int id2 = -1*rand() - 1;
    int id = rand();
    time_t now;
    time(&now);
    struct tm t = *localtime(&now);
    t.tm_mon += 1;
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient* person = new Patient(id, sur, n, pat, date);
    QVERIFY_EXCEPTION_THROWN(Appointment ap(id1, person, id2, t), logic_error);
}

void appointmen_test::Test_Constructor5()
{
    srand(time(NULL));
    int id1 = rand();
    int id2 = rand();
    int id = rand();
    time_t now;
    time(&now);
    struct tm t = *localtime(&now);
    t.tm_year -= 1;
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient* person = new Patient(id, sur, n, pat, date);
    QVERIFY_EXCEPTION_THROWN(Appointment ap(id1, person, id2, t), logic_error);
}

void appointmen_test::Test_Constructor6()
{
    srand(time(NULL));
    int id1 = rand();
    int id2 = rand();
    int id = rand();
    time_t now;
    time(&now);
    struct tm t = *localtime(&now);
    t.tm_mday -= 1;
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient* person = new Patient(id, sur, n, pat, date);
    QVERIFY_EXCEPTION_THROWN(Appointment ap(id1, person, id2, t), logic_error);
}

void appointmen_test::Test_Constructor7()
{
    srand(time(NULL));
    int id1 = rand();
    int id2 = rand();
    int id = rand();
    time_t now;
    time(&now);
    struct tm t = *localtime(&now);
    t.tm_hour -= 1;
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient* person = new Patient(id, sur, n, pat, date);
    QVERIFY_EXCEPTION_THROWN(Appointment ap(id1, person, id2, t), logic_error);
}

void appointmen_test::Test_Constructor8()
{
    srand(time(NULL));
    int id1 = rand();
    int id2 = rand();
    int id = rand();
    time_t now;
    time(&now);
    struct tm t = *localtime(&now);
    t.tm_min -= 1;
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient* person = new Patient(id, sur, n, pat, date);
    QVERIFY_EXCEPTION_THROWN(Appointment ap(id1, person, id2, t), logic_error);
}

void appointmen_test::Test_Constructor9()
{
    srand(time(NULL));
    int id1 = rand();
    QVERIFY_EXCEPTION_THROWN(Appointment ap(id1), logic_error);
}

void appointmen_test::Test_ToDeny1()
{
    srand(time(NULL));
    int id1 = rand();
    int id2 = rand();
    int id = rand();
    time_t now;
    time(&now);
    struct tm t = *localtime(&now);
    t.tm_hour += 1;
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient* person = new Patient(id, sur, n, pat, date);
    Appointment ap(id1, person, id2, t);
    QCOMPARE(false, ap.Deny());
    ap.ToDeny();
    QCOMPARE(true, ap.Deny());
}

void appointmen_test::Test_ToDeny2()
{
    srand(time(NULL));
    int id1 = rand();
    int id2 = rand();
    int id = rand();
    time_t now;
    time(&now);
    struct tm t = *localtime(&now);
    t.tm_hour += 1;
    string n = "name";
    string sur = "surname";
    string pat = "patronymic";
    Birth_date date = Birth_date();
    date.day = rand() % 31 + 1;
    date.month = rand() % 12 + 1;
    date.year = rand() % 2018 + 1;
    Patient* person = new Patient(id, sur, n, pat, date);
    Appointment ap(id1, person, id2, t);
    QCOMPARE(false, ap.Deny());
    ap.ToDeny();
    QCOMPARE(true, ap.Deny());
    ap.ToDeny();
    QCOMPARE(true, ap.Deny());
}

QTEST_APPLESS_MAIN(appointmen_test)

#include "tst_appointmen_test.moc"
