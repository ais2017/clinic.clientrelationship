#ifndef APPOINTMENT_H
#define APPOINTMENT_H

class Patient;

class Appointment
{
private:
    int id;
    Patient* person;
    int doctor_id;
    struct tm ap_time;
    bool deny;
public:
    Appointment(int = -1, Patient* = nullptr , int = -1, struct tm = tm());
    ~Appointment();
    bool Deny();
    void ToDeny();
    Patient* GetPerson();
    int GetId();
    int GetId_D();
    struct tm GetTime();
};

#endif // APPOINTMENT_H
