#ifndef PATIENT_H
#define PATIENT_H

class Service;



struct Birth_date{
    int year;
    int month;
    int day;
};

class Patient
{
private:
    int id;
    string surname;
    string name;
    string patronymic;
    Birth_date date;
    vector<Service*> services;
public:
    Patient(int = -1, string = "", string = "", string = "", Birth_date = Birth_date());
    ~Patient();
    void ChangeName(string s);
    void ChangeSurname(string s);
    void ChangePatronymic(string s);
    void DeleteData();
    void AddService(Service* s);
    int GetId();
    string GetSurname();
    string GetName();
    string GetPatronymic();
    Birth_date GetDate();
    vector<Service*> GetServices();
};

#endif // PATIENT_H
